package proxy

import (
	"context"
	"fmt"
	"log"
	"net"
	"strconv"
	"syscall"

	"github.com/pion/transport/v2"
	"github.com/pion/transport/v2/stdnet"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/v2/common/util"
)

type ProtectedNet struct {
	stdnet.Net

	ProtectFn func(c syscall.RawConn)
}

// ListenPacket announces on the local network address.
func (n *ProtectedNet) ListenPacket(network string, address string) (net.PacketConn, error) {
	var lc net.ListenConfig
	lc.Control = func(_, _ string, c syscall.RawConn) error {
		n.ProtectFn(c)
		return nil
	}
	return lc.ListenPacket(context.Background(), network, address)
}

// ListenUDP acts like ListenPacket for UDP networks.
func (n *ProtectedNet) ListenUDP(network string, locAddr *net.UDPAddr) (transport.UDPConn, error) {
	ret, err := n.ListenPacket(network, locAddr.String())
	if err != nil {
		return nil, err
	}
	return ret.(transport.UDPConn), nil
}

// Dial connects to the address on the named network.
func (n *ProtectedNet) Dial(network, address string) (net.Conn, error) {
	dialer := util.SystemDialerForProtectFn(n.ProtectFn)
	return dialer.Dial(network, address)
}

// DialUDP acts like Dial for UDP networks.
func (n *ProtectedNet) DialUDP(network string, laddr, raddr *net.UDPAddr) (transport.UDPConn, error) {
	ret, err := n.Dial(network, raddr.String())
	if err != nil {
		return nil, err
	}
	return ret.(transport.UDPConn), nil
}

// DialTCP acts like Dial for TCP networks.
func (n *ProtectedNet) DialTCP(network string, laddr, raddr *net.TCPAddr) (transport.TCPConn, error) {
	ret, err := n.Dial(network, raddr.String())
	if err != nil {
		return nil, err
	}
	return ret.(transport.TCPConn), nil
}

// ListenTCP acts like Listen for TCP networks.
func (n *ProtectedNet) ListenTCP(network string, laddr *net.TCPAddr) (transport.TCPListener, error) {
	var lc net.ListenConfig
	lc.Control = func(_, _ string, c syscall.RawConn) error {
		n.ProtectFn(c)
		return nil
	}
	ret, err := lc.Listen(context.Background(), network, laddr.String())
	if err != nil {
		return nil, err
	}
	return ret.(transport.TCPListener), nil
}

// ResolveIPAddr returns an address of IP end point.
func (n *ProtectedNet) ResolveIPAddr(network, address string) (*net.IPAddr, error) {
	log.Printf("ProtectedNet::ResolveIPAddr(%s, %s)", network, address)
	dialer := util.SystemDialerForProtectFn(n.ProtectFn)
	resolver := net.Resolver{
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			log.Printf("resolver dialer(%s, %s)", network, address)
			return dialer.DialContext(ctx, network, "8.8.8.8:53") // FIXME FIXME FIXME
		},
		PreferGo: true,
	}
	addresses, err := resolver.LookupIPAddr(context.Background(), address)
	if err != nil {
		return nil, err
	}
	if len(addresses) == 0 {
		return nil, fmt.Errorf("no addresses for query")
	}
	return &addresses[0], nil
}

// ResolveUDPAddr returns an address of UDP end point.
func (n *ProtectedNet) ResolveUDPAddr(network, address string) (*net.UDPAddr, error) {
	host, port, err := net.SplitHostPort(address)
	if err != nil {
		return nil, err
	}
	portInt, err := strconv.Atoi(port)
	if err != nil {
		return nil, err
	}
	ip, err := n.ResolveIPAddr(network, host)
	if err != nil {
		return nil, err
	}
	return &net.UDPAddr{
		IP:   ip.IP,
		Zone: ip.Zone,
		Port: portInt,
	}, nil
}

// ResolveTCPAddr returns an address of TCP end point.
func (n *ProtectedNet) ResolveTCPAddr(network, address string) (*net.TCPAddr, error) {
	host, port, err := net.SplitHostPort(address)
	if err != nil {
		return nil, err
	}
	portInt, err := strconv.Atoi(port)
	if err != nil {
		return nil, err
	}
	ip, err := n.ResolveIPAddr(network, host)
	if err != nil {
		return nil, err
	}
	return &net.TCPAddr{
		IP:   ip.IP,
		Zone: ip.Zone,
		Port: portInt,
	}, nil
}
